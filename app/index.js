const express = require("express");
const http = require("http");

const app = express();

require("./bootstrap")(app);

app.get("/", (req, res) => {
  res.render("main");
});

module.exports = () => {
  const port = process.env.APP_PORT;
  app.listen(port, () => {
    console.log(`app run ${port}`);
  });
};
